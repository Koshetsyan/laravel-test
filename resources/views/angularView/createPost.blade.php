<div class="myform col-xs-12" >
	<form name="posts" class="form-horizontal" role="form" novalidate="" autocomplete="off" data-ng-submit="createPost(posts.$valid)">
		<div class="form-body" style="padding:30px" data-ng-init="setForm(posts)">
		<div ng-repeat="validate in validates">
			<div style="color: red" ><h4>((validate))</h4></div>
		</div>
	    <div class="form-group">
	    	<div style="color: green">((msg))</div>
	    </div>
	       <div class="form-group">
	            <h4>Title</h4>
	            <input type="text" data-ng-model="title" name="title" class="form-control" id="title" required placeholder="Title"/>
	            <label class="control-label error"
                   data-ng-if="isValidFieldField('title') && posts.title.$error.required">Please enter title.
              	</label>
	        </div>

	       	<div class="form-group">
	            <h4>Title</h4>
	            <input type="text" data-ng-model="text" name="text" class="form-control" id="text" required placeholder="Text"/>
	            <label class="control-label error"
                   data-ng-if="isValidFieldField('text') && posts.text.$error.required">Please enter text.
              	</label>
	        </div>
	    <div class="form-group">
	    <center><button   class="btn btn-success " ng-disabled="posts.$invalid" >Add Post</button></center>
	    </div>
	</form>
</div>