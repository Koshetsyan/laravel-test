@extends('layouts.app')

@section('content')
<head>
    <!--bootstrap-->
    <!--bootstrap-->

    <meta charset="UTF-8">
    <title>Index</title>
    <!--angularjs-->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-route.js"></script>
    <script src="{{URL::asset('angular/app.js')}}"></script>
    <script src="{{URL::asset('angular/contollers/CreatePostController.js')}}"></script>
    <script src="{{URL::asset('angular/contollers/showPostsController.js')}}"></script>
    <script src="{{URL::asset('angular/config/routConfig.js')}}"></script>
    <script src="{{URL::asset('angular/js/angular-ui-router/release/angular-ui-router.min.js')}}"></script>
    <!--angularjs-->

</head>
<body ng-app="myApp" >

    <div class="col-md-3" >

        <div class=" navbar navbar-default" >
            <ul class="nav ">
                <li ng-class="{ active: isActive('/')}"><a ui-sref="createPost" class="btn "><span style="color: brown;">Create</span></a></li>
                <li ng-class="{ active: isActive('/')}"><a ui-sref="showPosts" class="btn "><span style="color: brown;">Posts</span></a></li>
            </ul>
        </div>
    </div>
    <div  class='container ' >
        <div class="col-md-8">
            <ui-view>


            </ui-view>
                
            <!-- </div> -->
        </div>

    </div>
</body>
@stop