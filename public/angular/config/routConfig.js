app.config(
	["$stateProvider", "$urlRouterProvider","$locationProvider",function($stateProvider, $urlRouterProvider,$locationProvider) {
    $urlRouterProvider.otherwise('createPost');
	$locationProvider.hashPrefix("");
    $stateProvider
        .state('createPost', {
        	url: '/createPost',
            controller: 'CreatePostCtrl',
            templateUrl: 'myAngular/createPost'
        })
        .state('showPosts', {
        	url: '/showPosts',
            controller: 'ShowPostsCtrl',
            templateUrl: 'myAngular/showPosts'
        });

}
]);


