app.controller('ShowPostsCtrl', function($scope,$http) {

    function showPosts(){
        $http.get('show').then(function(response) {
            console.log(response.data);
                $scope.posts = response.data;
        });
    }
    showPosts();

    $scope.addComment = function(post_id){
        var  comment = $('#comment_'+post_id).val();
        $http.post('addComment', {
            comment : comment,
            post_id : post_id,
        }).then(function(response) {
            if(response.data.comment){
                showPosts();
            }else {

                $scope.validates = response.data.error;
            }

        });
        $scope.message ='';
    }
});