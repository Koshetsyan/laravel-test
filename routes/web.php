<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/myAngular', 'AngularController@index');

Route::get('/myAngular/createPost', 'AngularController@createPost');

Route::get('/myAngular/showPosts', 'PostsController@showPosts');

Route::get('/show', 'PostsController@show');

Route::post('/addPost', 'PostsController@addPost');

Route::post('/addComment', 'CommentsController@addComment');



