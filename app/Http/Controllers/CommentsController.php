<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comment;

use Auth;

use Validator;

class CommentsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addComment(Request $request)
    {


    	$comment = $request['comment'];
    	$post_id = $request['post_id'];
        $rules = array(
            'comment'          => 'required',
            'post_id'   => 'required'
        );


       $validator = Validator::make(array(
            'comment'=> $comment,
            'post_id'=> $post_id        
        ), $rules);

		if($validator->passes()){

	    	$user_id = Auth::id();
	    	$comment = $request['comment'];
	    	$post_id = $request['post_id'];
	        $comment =  Comment::create([
	            'comment' => $comment,
	            'post_id' => $post_id,
	            'user_id' => $user_id
	        ]);

	        return response()->json([
			    'comment' => $comment
			]);
        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
}
