<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use DB;

use App\Post;

use Validator;
class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addPost(Request $request)
    {



    	$title = $request['title'];
    	$text = $request['text'];

        $rules = array(
            'title'   => 'required',
            'text'   => 'required'
        );


       $validator = Validator::make(array(
            'text'=> $text,
            'title'=> $title        
        ), $rules);

        $user_id = Auth::id();


        if($validator->passes()){
            $post =  Post::create([
                'title' => $title,
                'text' => $text,
                'user_id' => $user_id
            ]);

            return response()->json([
    		    'post' => $post
    		]);
        }else{
            return response()->json(['error'=>$validator->errors()->all()]);
        }
    }


    public function showPosts(Request $request)
    {
 		return view('angularView/showPosts');
            // var_dump($posts);
    }

    public function show()
    {


		$posts_users = DB::table('posts')
			->select('posts.id as post_id', 'posts.title' ,'posts.text', 'users.*')
            ->leftJoin('users', 'users.id', '=', 'posts.user_id')
         	->get();

 		$arrayData = array();
        foreach ($posts_users as $key => $post_user) {
			$comments_user = DB::table('comments')
			->select('comments.id as comment_id', 'comments.comment', 'users.*')
            ->leftJoin('users', 'users.id', '=', 'comments.user_id')
            ->where('comments.post_id', '=' , $post_user->post_id)
         	->get();
         	if($comments_user){
         		$post_user->comments = $comments_user;
         	}

            $arrayData[]= $post_user;
 		}
        return response()->json([
		    'post' => $arrayData
		]);
    }
}
