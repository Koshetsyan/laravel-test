<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = array('id', 'user_id', 'text', 'title' );

    public function users(){

        return $this->belongsTo('App\User');
    }
    
    public function comment(){
        return $this->hasMany('App\Comment');
    }
}
