<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = array('id', 'user_id', 'post_id', 'comment' );

    public function users(){

        return $this->belongsTo('App\User');
    }
    public function Post(){

        return $this->belongsTo('App\Post');
    }
}
